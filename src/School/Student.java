
package School;

import java.util.Scanner;

public class Student {
    String name;
    int[] scores = new int[5];

    /**
     * @param scores list of scores from the student
     * @return average score as double
     */
    public double avg (int[] scores) {
        double avg = 0;
        for (int score : scores) {
            avg += score;
        }
        return avg / scores.length;
    }

    /**
     * @param avg average of scores from avg()
     * @return char grade of student A B C D E F
     */
    public String score (double avg) {
        String grade = null;
        if (avg <= 50 ) {
            grade = "F";
        } else if (avg <= 60 ) {
            grade = "E";
        } else if (avg <= 70) {
            grade = "D";
        } else if (avg <= 80) {
            grade = "C";
        } else if (avg <= 90) {
            grade = "B";
        } else if (avg <= 100) {
            grade = "A";
        }
        return grade;
    }

    public void print_results (String name, double avg, int[] scores) {
        System.out.println("Nombre del Alumno: " + name);

        for (int i = 0; i < scores.length -1; i++) {
            System.out.println("Calificación "+ i + 1 + ": " + scores[i]);
        }

        System.out.println("Promedio: " + this.score(avg));
        System.out.println("Calificación: " + avg);
    }

    public void setCalificaciones () {
        int i = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nombre del Alumno: ");
        name = scanner.nextLine();

        do {
            System.out.print("Calificacion " + ( i+ 1 ) + " : " );
            scores[i] = scanner.nextInt();
            i++;
        } while (i < scores.length);

        this.print_results(name, avg(scores), scores);
    }
}
